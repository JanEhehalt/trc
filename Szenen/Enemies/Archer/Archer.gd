extends KinematicBody2D

var ArrowScene = preload("res://Szenen/Enemies/Archer/Arrow.tscn")

export var speed = 120

export var attackRadMin = 150
export var attackRadMax = 200

var player
var movement

func _ready():
	self.player = get_node("../../").getPlayer()

func _process(delta):
	if $Data.alive:
		act()
	else:
		if $AnimatedSprite.animation != "dead":
			$AnimatedSprite.frame = 0
			$AnimatedSprite.animation = "dead"
		else:
			if $AnimatedSprite.frame == 9:
				$AnimatedSprite.playing = false


func act():
	movement = Vector2()
	movement.x = player.position.x - position.x
	movement.y = player.position.y - position.y
	movement = movement.normalized() * speed
	
	if(position.distance_to(player.position) > attackRadMax):
		pass
	elif(position.distance_to(player.position) < attackRadMin):
		movement.x = -movement.x * 0.6
		movement.y = -movement.y * 0.6
	else:
		movement *= 0
	move_and_slide(movement)
	
	if player.position.x - position.x > 25:
		$AnimatedSprite.flip_h = false
	elif player.position.x - position.x < -25:
		$AnimatedSprite.flip_h = true
	
	if position.distance_to(player.position) < attackRadMax and position.distance_to(player.position) > attackRadMin and $AnimatedSprite.animation != "attack":
		$AnimatedSprite.animation = "attack"
	if $AnimatedSprite.animation == "attack":
		if $AnimatedSprite.frame == 6:
			# -> spawn Arrow Node
			var Arrow = ArrowScene.instance()
			var startPos
			var goalPos = player.position
			goalPos.y -= 8
			if $AnimatedSprite.flip_h:
				startPos = $LeftArrow.global_position
			else:
				startPos = $RightArrow.global_position
			var vector = Vector2()
			Arrow.position = startPos
			vector.x = goalPos.x - startPos.x
			vector.y = goalPos.y - startPos.y
			Arrow.movement = vector.normalized()
			Arrow.rotation = startPos.angle_to_point(goalPos)+PI
			get_parent().add_child(Arrow)
			$AnimatedSprite.frame += 1
		elif $AnimatedSprite.frame == 9:
			$AnimatedSprite.animation = "walk"
	elif movement.x != 0 or movement.y != 0:
		$AnimatedSprite.animation = "walk"
	else:
		$AnimatedSprite.animation = "idle"
	
