extends Area2D

export var speed = 250

export var lifetime = 0.8

var counter = 0

var movement = Vector2()

func _process(delta):
	counter += delta
	position.x += speed * movement.x * delta
	position.y += speed * movement.y * delta
	var hit = attack()
	if counter >= lifetime or hit == true:
		get_parent().remove_child(self)

func attack():
	for body in get_overlapping_bodies():
		if body.get_name() == "Player":
			print("HIT by ", get_name())
			return true
	return false
