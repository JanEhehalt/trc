extends KinematicBody2D

export var speed = 100

var player
var movement

func _ready():
	self.player = get_node("../../").getPlayer()
	
func _process(delta):
	if $Data.alive:
		act()
	else:
		if $AnimatedSprite.animation != "dead":
			$AnimatedSprite.frame = 0
			$AnimatedSprite.animation = "dead"
		else:
			if $AnimatedSprite.frame == 9:
				$AnimatedSprite.playing = false
		
		
func act():
	movement = Vector2()
	movement.x = player.position.x - position.x
	movement.y = player.position.y - position.y
	
	movement = movement.normalized() * speed
	
	move_and_slide(movement)
	
	if movement.x < 0:
		$AnimatedSprite.flip_h = true
	elif movement.x > 0:
		$AnimatedSprite.flip_h = false
	if position.distance_to(player.position) < 50 and $AnimatedSprite.animation != "attack":
		$AnimatedSprite.animation = "attack"
	if $AnimatedSprite.animation == "attack":
		if $AnimatedSprite.frame == 6:
			# TODO: Attack in this frame
			var attacked = false
			if $AnimatedSprite.flip_h:
				attacked = $LeftAttack.attack()
			if !$AnimatedSprite.flip_h:
				attacked = $RightAttack.attack()
			if attacked:
				$AnimatedSprite.frame += 1
		elif $AnimatedSprite.frame == 9:
			$AnimatedSprite.animation = "walk"
	elif movement.x != 0 or movement.y != 0:
		$AnimatedSprite.animation = "walk"
	else:
		$AnimatedSprite.animation = "idle"
	
