extends Area2D

func attack():
	for body in get_overlapping_areas():
		if body.get_name() == "PlayerHitbox":
			print("HIT by ", get_parent().get_name())
			return true
	return false
