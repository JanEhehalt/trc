extends Node2D

var KnightScene = preload("res://Szenen/Enemies/Knight/Knight.tscn")
var ArcherScene = preload("res://Szenen/Enemies/Archer/Archer.tscn")


var player
var newKnight1
var newKnight2
var newArcher1

var entities = Array()

func _process(delta):
	pass
	
func setPlayer(player):
	self.player = player
	self.player.position.x = 500
	self.player.position.y = 270
	self.player.visible = true
	entities.append(self.player)

func getPlayer():
	return self.player
	
func childPlayer():
	self.player.position.x = 500
	self.player.position.y = 270
	$Objects.add_child(self.player)

func unchildPlayer():
	$Objects.remove_child(player)

