extends KinematicBody2D

export var speed = 300
var parentRoom

func _process(delta):
	var movement = Vector2()
	
	# 1: full right, -1: full left
	var horizontal = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	# 1: full down, -1: full up
	var vertical = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	
	movement.x = horizontal
	movement.y = vertical
	#print(horizontal , " " , vertical)
	movement *= speed
	
	move_and_slide(movement)
	
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if collision.collider.name == "TopDoor":
			parentRoom.roomChange("up")
		elif collision.collider.name == "LeftDoor":
			parentRoom.roomChange("left")
		elif collision.collider.name == "DownDoor":
			parentRoom.roomChange("down")
		elif collision.collider.name == "RightDoor":
			parentRoom.roomChange("right")
	
	if movement.x < 0:
		$AnimatedSprite.flip_h = true
	elif movement.x > 0:
		$AnimatedSprite.flip_h = false
		
	var attackNotRunning = get_node("AnimatedSprite").animation == "attack" && get_node("AnimatedSprite").frame == 9
	if $AnimatedSprite.animation == "attack" && $AnimatedSprite.frame == 6:
		attack()
		$AnimatedSprite.frame += 1
		
	if Input.is_action_pressed("ui_select"):
		get_node("AnimatedSprite").animation = "attack"
		
	elif attackNotRunning:
		if movement.x != 0 or movement.y != 0:
			get_node("AnimatedSprite").animation = "walk"
		else:
			get_node("AnimatedSprite").animation = "idle"

func manageAttack(attacker):
	print("Angegriffen von", attacker)
	
func attack():
	var hitbox
	if $AnimatedSprite.flip_h:
		hitbox = $LeftAttack
	else:
		hitbox = $RightAttack
		
	for body in hitbox.get_overlapping_areas():
		if body.get_name() == "Hitbox":
			print("Player hit ", body.get_node("../").name)
			body.get_node("../Data").hp -= 5
			return true
	return false
