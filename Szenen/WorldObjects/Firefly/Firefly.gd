extends Node2D

var counter = 0

var noise = OpenSimplexNoise.new()

onready var energyBase = $Light2D.energy
onready var colorBase = $Light2D.color.r * 255

func _init():
	noise.seed = randi()
	noise.octaves = 4
	noise.period = 20.0
	noise.persistence = 0.8
	
func _process(delta):
	counter += delta*3
	$Particles2D.process_material.color.b = (abs(noise.get_noise_1d(counter)) * 140) / 255
	$Light2D.color.r = (colorBase + (noise.get_noise_1d(counter)*10)) / 255
	$Light2D.energy = energyBase + abs(noise.get_noise_1d(counter))
