extends Light2D

var counter = 0

var noise = OpenSimplexNoise.new()

onready var energyBase = energy
onready var colorBase = color.r*255

func _init():
	noise.seed = randi()
	noise.octaves = 4
	noise.period = 20.0
	noise.persistence = 0.8
	
	
func _process(delta):
	counter += delta * 4
	#if energy != null and colorBase != null:
	energy = energyBase + abs(noise.get_noise_1d(counter))
	color.r = (colorBase + (noise.get_noise_1d(counter)*10)) / 255
