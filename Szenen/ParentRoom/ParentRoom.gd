extends Node2D

var TestRoomScene = preload("res://Szenen/Rooms/TestRoom.tscn")
var testRoom
var room

var PlayerScene = preload("res://Szenen/Player/Player.tscn")
var player

var matrix = []
var currentX = -1
var currentY = -1

func _ready():
	player = PlayerScene.instance()
	player.parentRoom = self
	
	generateRooms()

# up, left, down, right
func roomChange(direction):
	match direction:
		"up":
			#if currentY > 0 and matrix[currentX][currentY-1] != null:
				currentY -= 1
			#else:
				#return
		"down":
			#if currentY < 4 and matrix[currentX][currentY+1] != null:
				currentY += 1
			#else:
				#return
		"left":
			#if currentX > 0 and matrix[currentX-1][currentY] != null:
				currentX -= 1
			#else:
				#return
		"right":
			#if currentX < 4 and matrix[currentX+1][currentY] != null:
				currentX += 1
			#else:
				#return
		_:
			return
			
	room.unchildPlayer()
	remove_child(testRoom)
	testRoom = matrix[currentX][currentY]
	room = testRoom.get_child(0)
	room.childPlayer()
	add_child(testRoom)

func generateRooms():
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var roomAmount = rng.randi_range(5, 10)
	
	for xPos in range(roomAmount):
		matrix.append([])
		for _yPos in range(roomAmount):
			#var tempRoom = TestRoomScene.instance()
			#tempRoom.get_child(0).setPlayer(player)
			matrix[xPos].append(null)
	
	currentX = rng.randi_range(0, roomAmount - 1)
	currentY = rng.randi_range(0, roomAmount - 1)
	var tempRoom = TestRoomScene.instance()
	tempRoom.get_child(0).setPlayer(player)
	matrix[currentX][currentY] = tempRoom
	
	# 0: up, 1: left, 2: down, 3: right
	var direction
	for i in range(roomAmount):
		direction = rng.randi_range(0, 3)
		
		if direction == 0 and currentY > 0:
			currentY -= 1
		elif direction == 1 and currentX > 0:
			currentX -= 1
		elif direction == 2 and currentY < roomAmount - 1:
			currentY += 1
		elif direction == 3 and currentX < roomAmount - 1:
			currentX += 1
		else:
			i -= 1
			continue
	
		tempRoom = TestRoomScene.instance()
		tempRoom.get_child(0).setPlayer(player)
		matrix[currentX][currentY] = tempRoom
		
	testRoom = matrix[currentX][currentY]
	room = testRoom.get_child(0)
	room.childPlayer()
	add_child(testRoom)
	
	for j in range(matrix.size()):
		var output = ""
		for i in range(matrix[0].size()):
			if matrix[i][j] != null:
				output += "1 "
			else:
				output += "0 "
		print(output)
	
	for i in range(matrix.size()):
		for j in range(matrix[0].size()):
			if matrix[i][j] != null:
				if !j > 0:
					matrix[i][j].get_child(0).remove_child(matrix[i][j].get_child(0).get_node("TopDoor"))
				if !i > 0:
					matrix[i][j].get_child(0).remove_child(matrix[i][j].get_child(0).get_node("LeftDoor"))
				if !j < roomAmount - 1:
					matrix[i][j].get_child(0).remove_child(matrix[i][j].get_child(0).get_node("DownDoor"))
				if !i < roomAmount - 1:
					matrix[i][j].get_child(0).remove_child(matrix[i][j].get_child(0).get_node("RightDoor"))
	
	for i in range(matrix.size()):
		for j in range(matrix[0].size()):
			if matrix[i][j] != null:
				if j > 0 and matrix[i][j-1] == null:
					matrix[i][j].get_child(0).remove_child(matrix[i][j].get_child(0).get_node("TopDoor"))
				if i > 0 and matrix[i-1][j] == null:
					matrix[i][j].get_child(0).remove_child(matrix[i][j].get_child(0).get_node("LeftDoor"))
				if j < roomAmount - 1 and matrix[i][j+1] == null:
					matrix[i][j].get_child(0).remove_child(matrix[i][j].get_child(0).get_node("DownDoor"))
				if i < roomAmount - 1 and matrix[i+1][j] == null:
					matrix[i][j].get_child(0).remove_child(matrix[i][j].get_child(0).get_node("RightDoor"))
